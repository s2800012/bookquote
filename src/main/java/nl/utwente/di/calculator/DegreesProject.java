package nl.utwente.di.calculator;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class DegreesProject extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private FahrenheitCalculator calculator;
	
    public void init() throws ServletException {
    	calculator = new FahrenheitCalculator();
    }
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType =
      "<!DOCTYPE HTML>\n";
    String title = "Fahrenheit Calculator";
    out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Celcius: " +
                   request.getParameter("celcius") + "\n" +
                "  <P>Price: " +
                   Double.toString(calculator.getFahrenheit(request.getParameter("celcius"))) +
                "</BODY></HTML>");
  }
  

}
