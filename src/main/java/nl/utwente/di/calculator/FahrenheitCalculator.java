package nl.utwente.di.calculator;

public class FahrenheitCalculator {
    public double getFahrenheit(String celcius) {
        double value = Double.parseDouble(celcius);
        return ((value * 1.8) + 32);
    }
}
